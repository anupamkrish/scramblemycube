import argparse
import random


def argParsing():
    parser = argparse.ArgumentParser()  # Init arg parser
    # Set optional args
    parser.add_argument("-s", "--scrambles", help="Set number of scrambles between 21-50", type=int, default=20)
    parser.add_argument("-r", "--readable", help="Print in readable format", action='store_true')
    args = parser.parse_args()          # Parse args
    return args


# The horrible logic
def horribleLogic(n):
    notations1 = ["L", "R", "U", "D", "F", "B"]
    notations2 = [x + "2" for x in notations1]
    notations3 = [x + "'" for x in notations1]
    notations4 = [x + "2'" for x in notations1]
    notations_all = notations1 + notations2 + notations3 + notations4
    randomlist = random.sample(notations_all, n)
    return randomlist


# Prints 5 notations every line
def readable(n):
    rand = horribleLogic(n)
    l = []
    for index, instruction in enumerate(rand, start=1):
        l.append(instruction)
        if index % 5 == 0:
            print(*l)
            l = []
        elif index == len(rand):
            print(*l)


# Print in single line
def normalPrint(n):
    print(*horribleLogic(n))


# Driver code
if __name__ == "__main__":
    n = argParsing().scrambles
    r = argParsing().readable
    if n not in range(20, 50):
        print("Enter a number between 21-50")
        exit()
    readable(n) if r else normalPrint(n)