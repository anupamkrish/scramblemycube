# The Horrible Rubik's Cube Scrambler

Every once in a while I would visit [ruwix.com](https://ruwix.com/puzzle-scramble-generators/rubiks-cube-scrambler/) to scramble my Rubik's cube (It distracts me everytime I notice two consecutive colors when attempting to scramble it myself without using a computer.)
Using a computer generated algorithm gives the assurance of there being no human bias. But visiting a webpage to scamble a Rubik's cube became all too cumbersome.

I wrote this horrible program that can give you instructions to scramble your Rubik's cube. It will work on any 3x3 cube. To make things easier, you can add this line to the end of your .bashrc config file just like I did.
```
alias scramblemycube="python ~/scramblemycube/rubiks_scramble.py"
```

Type the following into your terminal to scramble your Rubik's cube.
```
scramblemycube
```

To display list of supported commands and get help, use the **-h** flag
```
scramblemycube --help
```

For information on decoding Rubik's cube notations, [click here](https://medium.com/@maxdeutsch/m2m-day-69-decoding-rubiks-cube-algorithms-6ea7e7704ec9).
